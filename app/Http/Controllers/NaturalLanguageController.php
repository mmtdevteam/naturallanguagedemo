<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JoggApp\NaturalLanguage\NaturalLanguageClient;

class NaturalLanguageController extends Controller
{
    public function getSentiment(Request $request)
    {
        $nlcService = (new NaturalLanguageClient(config('naturallanguage')));

        $content = $request->content;

        return view('sentiment', [
            'raw'   => $content,
            'data'  => (object)[
                'sentiment'         => $nlcService->sentiment($content),
                'sentimentSentence' => $this->getSentimentBySentence($nlcService, $content),
                'entitySentiment'   => $this->sortScoreDescEntity($nlcService->entitySentiment($content))
            ]
        ]);
    }

    public function sortScoreDescEntity($data)
    {
        usort($data, function ($a, $b) {
            return strcmp($b['sentiment']['score'], $a['sentiment']['score']);
        });
        return $data;
    }

    public function getSentimentBySentence($nlcService, $content)
    {
        $content = $this->cleanExplode($content);
        $result  = [];

        foreach ($content as $value) {
            $value = $value.'.';
            $result[$value] = $nlcService->sentiment($value);
        }

        return $result;
    }

    public function cleanExplode($text, $lowercase = false)
    {
        return preg_split("/\.(?=\s|$)/m", $text, -1, PREG_SPLIT_NO_EMPTY);
    }
}
