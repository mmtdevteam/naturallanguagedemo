<?php

namespace App\Http\Controllers;

use PhpScience\TextRank\Tool\StopWords\English;
use PhpScience\TextRank\TextRankFacade;
use PhpScience\TextRank\Tool\Summarize;
use App\Services\Summarizer;
use DivineOmega\PHPSummary\SummaryTool;
use Illuminate\Http\Request;

use JoggApp\NaturalLanguage\NaturalLanguage;
use JoggApp\NaturalLanguage\NaturalLanguageClient;

class SummarizerController extends Controller
{
    public function test(Request $request)
    {
        $text = $request->text;

        $data =  (object) [
            'raw'        => $text,
            'text1'  => array_values($this->summarizeByPackage1($text)),
            'text2' => $this->summarizeByPackage2($text),
            'text3' => $this->summarizeByPackage3($text),
        ];

        return view('welcome', ['data' => $data]);
    }

    public function summarizeByPackage1($text)
    {
        //composer require php-science/textrank
        $textRank = new TextRankFacade();
        $textRank->setStopWords(new English());

        return $textRank->summarizeTextFreely(
            $text,
            5,
            1,
            Summarize::GET_ALL_IMPORTANT
        );
    }

    public function summarizeByPackage2($text)
    {
        //https://github.com/freekrai/summarizer
        return (new Summarizer())->get_summary($text);
    }

    public function summarizeByPackage3($text)
    {
        // composer require divineomega/php-summary
        return (new SummaryTool($text))->getSummary();
    }
}
