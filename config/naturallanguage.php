<?php
return [
    /*
    |--------------------------------------------------------------------------
    | The id of project created in the Google Cloud Platform console.
    |--------------------------------------------------------------------------
    */
    'project_id' => env('NATURAL_LANGUAGE_PROJECT_ID', 'api2-1574477938459'),

    /*
    |--------------------------------------------------------------------------
    | Path to the json file containing the authentication credentials.
    |--------------------------------------------------------------------------
    */
    'key_file_path' =>  base_path('storage\google.json'),
];
