<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Laravel</title>

    <!-- Fonts -->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>




</head>

<body class="container-fluid">
    <div class="row">
        <div class="col-6 mx-auto my-5">
            <form method="post" action="/test" style="text-align: center">
                @csrf
                <textarea required class="form-control" name="text" rows="15" placeholder="Nhập nội dung"
                    >@isset($data->raw) {{ $data->raw }} @endisset</textarea>
                <div class="mt-3">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
    @isset($data)
        <div class="row justify-content-center">
            <div class="col-4">
                <textarea readonly rows="10" class="form-control">{{ $data->text1[0] }}</textarea>
            </div>
            <div class="col-4">
                <textarea readonly rows="10" class="form-control">{{ $data->text2 }}</textarea>
            </div>
            <div class="col-4">
                <textarea readonly rows="10" class="form-control">{{ $data->text3 }}</textarea>
            </div>
        </div>
    @endisset
    </div>
    </div>
</body>

</html>
