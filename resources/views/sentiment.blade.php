<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Laravel</title>

    <!-- Fonts -->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>




</head>

<body class="container-fluid">
    <div class="row">
        <div class="col-6 mx-auto my-5">
            <form method="post" action="/getSentiment" style="text-align: center">
                @csrf
                <textarea required class="form-control" name="content" rows="15"
                    placeholder="Nhập nội dung">@isset($raw) {{ $raw }} @endisset</textarea>
                <div class="mt-3">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
    @isset($data)
        <div class="row justify-content-center">
            <div class="col-5 border p-4">
                <h4 class="mb-2">Document and Sentence Level Sentiment </h4>
                <div class="d-flex mb-2">
                    <div class="me-auto w-75"> </div>
                    <div class="align-self-center">
                        <div class="d-inline-block" style="width:70px">Score</div>
                        <div class="d-inline-block" style="width:70px">Magnitude</div>
                    </div>
                </div>
                <div class="d-flex mb-3">
                    <div class="me-auto w-75"> <span style="font-weight: 500">Entire Document:</span> </div>
                    <div class="align-self-center">
                        @php
                            $score = $data->sentiment['score'];
                        @endphp
                        <span class="badge
                                            @if ($score>= 0.25 && $score <= 1) bg-success @endif @if ($score >= -0.25 && $score <= 0.25)
                                bg-warning
                                @endif
                                @if ($score >= -1 && $score <= -0.25) bg-danger @endif d-inline-block  p-2" style="width:70px">{{ $score }}</span>
                        <span class="badge bg-primary d-inline-block p-2"
                            style="width:70px">{{ $data->sentiment['magnitude'] }}</span>
                    </div>
                </div>

                @foreach ($data->sentimentSentence as $key => $value)
                    <div class="d-flex mb-2">
                        <div class="me-auto w-75">- {{ $key }} </div>
                        <div class="align-self-center">
                            @php
                                $score = $value['score'];
                            @endphp
                            <span class="badge
                                            @if ($score>= 0.25 && $score <= 1) bg-success @endif @if ($score >= -0.25 && $score <= 0.25)
                                    bg-warning
                @endif
                @if ($score >= -1 && $score <= -0.25) bg-danger @endif
                    d-inline-block p-2" style="width:70px">{{ $score }}</span>
                    <span class="badge bg-primary d-inline-block  p-2" style="width:70px">{{ $value['magnitude'] }}</span>
            </div>
        </div>
        @endforeach
        <div class="my-5">
            <div class="d-flex justify-content-center">
                <div class="pe-3 align-self-center">
                    <strong>Score Range:</strong>
                </div>
                <div>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-success">0.25 — 1.0
                        </button>
                        <button type="button" class="btn btn-warning">-0.25 — 0.25
                        </button>
                        <button type="button" class="btn btn-danger">-1.0 — -0.25
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="mt-3">
            <h4 class="mb-3">Entity Level Sentiment </h4>
            <div class="row row-cols-1 row-cols-md-2 g-4">
                @foreach ($data->entitySentiment as $index => $value)
                    <div class="col">
                        <div class="card">
                            <div class="position-absolute badge bg-secondary px-2" style="right: 0">{{ $value['type'] }}
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">{{ $index + 1 }}: {{ $value['name'] }}</h5>
                                <p class="card-text">
                                    <span class="text-secondary" style="font-weight: 500">Sentiment:</span>
                                    <span class="text-success pr-2" style="font-weight: 500"> Score</span>
                                    {{ $value['sentiment']['score'] }}
                                    <span class="text-success" style="font-weight: 500">Magnitude</span>
                                    {{ $value['sentiment']['magnitude'] }}

                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        </div>
    @endisset
    </div>
    </div>
</body>

</html>
