<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SummarizerController;
use App\Http\Controllers\NaturalLanguageController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','welcome');
Route::view('/sentiment','sentiment');

Route::post('/test', [SummarizerController::class, 'test']);

Route::post('/getSentiment', [NaturalLanguageController::class, 'getSentiment']);

